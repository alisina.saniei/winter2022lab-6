public class Board{
 private Die first;
 private Die second;
 private boolean[] closedTiles;
 
 public Board (){
  this.first = new Die();
  this.second = new Die();
  
  this.closedTiles = new boolean[12];
  for (int i = 0;i < this.closedTiles.length;i++){
   this.closedTiles[i] = false;
  }
 }
 public String toString(){
  String dice = "";
  for(int i = 0;i<this.closedTiles.length;i++){
   if (!this.closedTiles[i]){
    dice = dice + (i+1) + " ";
   }
   else{
    dice = dice + "x ";
   }
  }
  return dice;
 }
 public boolean playATurn(){
  this.first.roll();
  this.second.roll();
  
  System.out.println(this.first.toString() + "," + this.second.toString());
  int sum = first.getPips() + second.getPips();
  boolean gameOver = false;
  
  if (this.closedTiles[sum-1]){
   System.out.println("Shut");
   gameOver = true;
  }
  else {
   this.closedTiles[sum-1] = true;
   System.out.println("closing Tiles" + (sum));
   gameOver = false;
  }
  return gameOver;
 }
}